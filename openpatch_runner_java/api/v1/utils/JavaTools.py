import re
import subprocess
import time

from openpatch_runner_java.api.v1.utils.JavaSDL import JavaSDL


class JavaToolchainFacade:
    """JavaToolchainFacade (JTF) encapsulates all of java's toolchain."""
    _logger = None

    def __init__(self, logger=None):
        self._logger = logger

    def run(self, javasdl, fqcn_main, sourcetype):
        """Executes a Java class file.

        :param javasdl: (JavaSDL) A java standard directory layout, in which the class will be executed.
        :param fqcn_main: (String) Fully qualified class name of the main class.
        :param sourcetype: (JavaSDL.SourceType) Whether given FQCN is 'TEST' or 'SOURCE'.
        :raises RuntimeError: upon non-existant given sourcetype.
        :return: (dict) with the call, the exitcode, stdout, stderr and the execution time.
        """
        if sourcetype == JavaSDL.SourceType.SOURCE:
            return self.call_subprocess(
                "java -cp " + javasdl.get_classpath() + " " + fqcn_main,
                cwd=javasdl.get_src_dir())
        elif sourcetype == JavaSDL.SourceType.TEST:
            return self.call_subprocess(
                "java -cp " + javasdl.get_classpath() +
                " org.junit.runner.JUnitCore " + fqcn_main,
                cwd=javasdl.get_test_dir())
        else:
            raise RuntimeError(
                "run(): Coding error / nonexistant-sourcetype given.")

    def compile_javac(self, javasdl, fqcn_main, sourcetype):
        """Compiles a java class.

        Dependencies are automatically resolved by the compiler.

        :param javasdl: (JavaSDL) A java standard directory layout, in which the classes will be compiled.
        :param fqcn_main: (String) Fully qualified class name of the main class.
        :param sourcetype: (JavaSDL.SourceType) Whether given FQCN is 'TEST' or 'SOURCE'.
        :return: (dict) with the call, the exitcode, stdout, stderr and the execution time.
        """
        call = "javac -cp " + javasdl.get_classpath() + " " \
               + javasdl.fqcn_to_path(fqcn_main) + \
            javasdl.get_java_filename(fqcn_main)

        if sourcetype == JavaSDL.SourceType.SOURCE:
            return self.call_subprocess(call, cwd=javasdl.get_src_dir())
        elif sourcetype == JavaSDL.SourceType.TEST:
            return self.call_subprocess(call, cwd=javasdl.get_test_dir())
        else:
            raise RuntimeError(
                "compile_javac(): Coding error / nonexistant-sourcetype given."
            )

    def call_subprocess(self, call, timeout=None, cwd=None):
        """Calls a subprocess. This is a wrapper method for all toolchain calls.

        :param call: (String) CLI-call to a program, which will be executed.
        :param timeout: (Int) A timeout, after which the execution will be terminated.
        :param cwd: (String) The current working directory in which context the call will be made.
        :return: (dict) with the call, the exitcode, stdout, stderr and the execution time.
        """
        if self._logger is not None:
            self._logger.debug("Calling subprocess: " + call)

        start_time = time.time()

        # Set priority of started commands to 'low' (i.e. nice value of 15)
        # We cannot effectively limit a docker container's resources without imposing strict guidelines
        # (e.g. "use only 10% of cpu time").
        call = "nice -n15 " + call

        cmd = subprocess.Popen([call],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True,
                               cwd=cwd)

        exit_code = cmd.wait(timeout)

        stop_time = time.time()
        cmd_out, cmd_err = cmd.communicate()

        if exit_code != 0 and self._logger is not None:
            self._logger.warn(
                [cmd_out.decode('utf-8'),
                 cmd_err.decode('utf-8')])

        return {
            "call": call,
            "exitcode": exit_code,
            "stdout": cmd_out.decode('utf-8'),
            "stderr": cmd_err.decode('utf-8'),
            "exectime": stop_time - start_time
        }


class JUnitConverter:
    """This class transforms the output of JUnit into something meaningful."""

    @staticmethod
    def convert_junit_output(raw_junit_output: str):
        """Parses the JUnitCore-cli-output into a meaningful dict.

        JUnit is executed on the cli with the following command:
        `"java -cp junit-4.12.jar org.junit.runner.JUnitCore [test-class]"`
        The result is passed to this function.

        :param raw_junit_output: (String) Raw cli-output of junit.
        :return: (dict) a meaningful transformation of the junit-output, e.g.
            parsed_dict = {
                "total_tests_run": 2,
                "total_tests_failed": 3,
                "values": [
                    {
                        "number": "1",
                        "message": "Should have printed 'World' not 'Cosmos'.",
                        "expected_value": "Hello[World]",
                        "actual_value": "Hello[Cosmos]",
                    }, {...}
                ]
            }
        """
        # if no input was given or test could not be compiled
        try:
            if re.search("java.lang.IllegalArgumentException: Could not find class", raw_junit_output) or \
                    len(raw_junit_output) == 0:
                return {
                    "error":
                    "Could not parse JUnit-output.",
                    "error_msg":
                    "Given junit_output was either empty, malformed or "
                    "the test did not compile at all.",
                    "junit_output":
                    raw_junit_output
                }

            if re.search('(0 tests)', raw_junit_output):
                return {
                    "error": "Could not parse JUnit-output.",
                    "error_msg": "No tests were provided!",
                    "junit_output": raw_junit_output
                }

            if re.search("FAILURES!!!", raw_junit_output):
                # split contains the ascii-stacktrace which is generated by executing
                # "java -cp junit-4.12.jar org.junit.runner.JUnitCore [test-class]" on the cli
                # We are going to parse this raw output in this method.
                # A word of warning: if junit is updated to version 5 in the future, this method has to be renewed.
                split = raw_junit_output.split('\n')
                # Remove all lines which start with '\t', i.e. are intended in the generated stacktrace
                split = [x for x in split if not x.startswith('\t')]
                # Remove all empty strings:
                split = [x for x in split if len(x) > 0]
                # Cut off junit version, error-string and time measurement and number of failures
                split = split[4:]

                # parsed to dictionary
                parsed_dict = {
                    "total_tests_run":
                    int(
                        re.search(r"Tests run: (\d+),  Failures: (\d+)",
                                  split[-1]).group(1)),
                    "total_tests_failed":
                    int(
                        re.search(r"Tests run: (\d+),  Failures: (\d+)",
                                  split[-1]).group(2)),
                    "is_correct":
                    False,
                    "values": []
                }

                for index, obj in enumerate(split):
                    # if object (str) begins with a number and a closing brace:
                    if re.search(r'\d+\)', obj):
                        # the next element (i.e. index+1) will contain the actual junit-result
                        test_dict = {
                            # cut off closing brace
                            "number": int(
                                re.search(r'\d+\)', obj).group()[:-1]),
                            "message": "",
                            "expected_value": "",
                            "actual_value": "",
                        }

                        # if the unittest has a custom message:
                        if re.search(r":.(.*).expected", split[index + 1]):
                            test_dict["message"] = re.search(
                                r":.(.*).expected", split[index + 1]).group(1)

                        # if we can extract the expected result:
                        if re.search(r"expected:<(.*)> ", split[index + 1]):
                            test_dict["expected_value"] = re.search(
                                r"expected:<(.*)> ", split[index + 1]).group(1)

                        # if we can extract the actual result:
                        if re.search(r"but was:<(.*)>", split[index + 1]):
                            test_dict["actual_value"] = re.search(
                                r"but was:<(.*)>", split[index + 1]).group(1)

                        parsed_dict["values"].append(test_dict)
                return parsed_dict
            else:
                return {
                    "total_tests_run":
                    int(
                        re.search(r"\((\d+) tests{0,1}\)",
                                  raw_junit_output).group(1)),
                    "total_tests_failed":
                    0,
                    "is_correct":
                    True,
                    "values": [],
                }
        except (AttributeError, IndexError) as e:
            # Something went wrong when parsing the junit output
            # Sorry. :-(
            return {
                "error":
                "Could not parse JUnit-output for unknown reason. "
                "'error_msg' contains the regex-exception.",
                "error_msg":
                str(e),
                "junit_output":
                raw_junit_output,
            }
