from flask import current_app, jsonify, request
from jsonschema import validate
from jsonschema.exceptions import SchemaError, ValidationError
from openpatch_runner_java.api.v1 import api
from openpatch_runner_java.api.v1.schemas.json_schemas import (
    request_schema,
    response_schema,
)
from openpatch_runner_java.api.v1.utils.HmacUtils import verify_request
from openpatch_runner_java.api.v1.utils.JavaSDL import JavaSDL
from openpatch_runner_java.api.v1.utils.JavaTools import (
    JavaToolchainFacade,
    JUnitConverter,
)

javasdl = None
log = None


@api.route("/run/post", methods=["POST"])
def run_post():
    """
    @api {post} /api/v1/run Execute Java-sourcecode.
    @apiVersion 1.0.0
    @apiName ExecuteJava
    @apiGroup Runner

    @apiPermission api

    @apiParamExample {json} Example Request to Commoop-Runner:
            {
                'payload':
                    {
                        'sources':
                            [
                                {
                                    'fqcn': 'mypkg.HelloWorld',
                                    'code': 'package mypkg;
                                             public class HelloWorld {
                                                 private static String msg = "HelloWorld";
                                                  public static void main (String[] args) {
                                                     System.out.println(msg);
                                                 }
                                                 public String getMsg(){
                                                     return msg;
                                                 }
                                             }'
                                }
                            ]
                        'tests':
                            [
                                {
                                    'fqcn': 'TestHelloWorld',
                                    'code': 'import mypkg.HelloWorld;
                                              import org.junit.Test;
                                              import static org.junit.Assert.assertEquals;

                                              public class HelloWorldTest {
                                                  HelloWorld hw = new HelloWorld();

                                                  [at]Test
                                                  public void testOkay {
                                                      assertEquals("HelloWorld", hw.getMsg());
                                                  }

                                                  [at]Test
                                                  public void failingTestOne() {
                                                      assertEquals("A custom message for the first failing test.",
                                                                   "HelloUniverse", hw.getMsg());
                                                  }

                                                  [at]Test
                                                  public void failingTestTwo() {
                                                      assertEquals("Another custom message for the second failing test.",
                                                                   "HelloCosmos", hw.getMsg());
                                                  }
                                              }'
                                 }
                             ]
                     }
             }

     @apiSuccess {json} runnerResult Results from compiling and executing Java-sources and -tests.
     @apiSuccessExample {json} Example Success-Response for a commoop-runner-java:
             {
               "src_compile": {
                 "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                 "exectime": 2.618844747543335,
                 "exitcode": 0,
                 "stderr": "",
                 "stdout": ""
               },
               "src_run": {
                 "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                 "exectime": 0.23933076858520508,
                 "exitcode": 0,
                 "stderr": "",
                 "stdout": "HelloWorld\n"
               },
               "test_compile": [
                 {
                   "call": "nice -n15 javac -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                   "exectime": 2.4677603244781494,
                   "exitcode": 0,
                   "stderr": "",
                   "stdout": ""
                 }
               ],
               "test_results": [
                 {
                   "is_correct": false,
                   "total_tests_failed": 1,
                   "total_tests_run": 2,
                   "values": [
                     {
                       "actual_value": "Hello[World]",
                       "expected_value": "Hello[FAIL]",
                       "message": "",
                       "number": 1
                     }
                   ]
                 }
               ],
               "test_run": [
                 {
                   "call": "nice -n15 java -cp /tmp/flask_pid-7_1516142044661/src/:/tmp/[...]",
                   "exectime": 0.7232999801635742,
                   "exitcode": 1,
                   "stderr": "",
                   "stdout": "JUnit version 4.12\n.E.\n [...] \nTests run: 2,  Failures: 1\n\n"
                 }
               ]
             }
     """
    global javasdl, log
    log = current_app.logger

    # Validate HMAC:
    try:
        verify_request(request)
    except RuntimeError as e:
        return jsonify({"error": str(e)})

    jtf = JavaToolchainFacade(log)
    javasdl = JavaSDL(logger=log)
    req = request.get_json()

    try:
        validate(req, request_schema)
    except ValidationError as ve:
        log.debug("An exception occurred during schema validation!\n" + str(ve))
    except SchemaError as se:
        log.debug("An exception occurred during schema validation!\n" + str(se))

    # We check for all errors. If any exception occurs, it is put into a json response to commoop-runner.
    # We want to prevent http 500 errors without any meaningful information at all costs.
    try:
        # Write contents of request to session_folder
        for sourcefile in req["sources"]:
            javasdl.write_javafile(
                sourcefile["fqcn"], sourcefile["code"], JavaSDL.SourceType.SOURCE
            )

        if "tests" in req:
            for testfile in req["tests"]:
                javasdl.write_javafile(
                    testfile["fqcn"], testfile["code"], JavaSDL.SourceType.TEST
                )

        # We only compile the first sourcefile. javac will pull all dependencies for us.
        src_compile = jtf.compile_javac(
            javasdl, req["sources"][0]["fqcn"], JavaSDL.SourceType.SOURCE
        )

        # Run the first sourcefile, which is the main file (see documentation!):
        src_run = jtf.run(javasdl, req["sources"][0]["fqcn"], JavaSDL.SourceType.SOURCE)

        test_runs = []
        test_compile = []
        test_results = []
        # Compile all tests, if any was provided.
        if "tests" in req:
            for testfile in req["tests"]:
                test_compile.append(
                    jtf.compile_javac(
                        javasdl, testfile["fqcn"], JavaSDL.SourceType.TEST
                    )
                )

            # Run all compiled tests.
            for test in req["tests"]:
                test_runs.append(
                    jtf.run(javasdl, test["fqcn"], JavaSDL.SourceType.TEST)
                )

            juc = JUnitConverter()
            for run in test_runs:
                test_results.append(juc.convert_junit_output(run["stdout"]))

        resp = {
            "src_compile": src_compile,
            "src_run": src_run,
            "test_compile": test_compile,
            "test_run": test_runs,
            "test_results": test_results,
        }
    except Exception as e:
        return jsonify(
            {
                "error": str(e),
                "type": e.__class__.__name__,
                "errormsg": "An error occured in openpatch-runner-java.",
            }
        )

    try:
        validate(req, response_schema)
    except (ValidationError, SchemaError) as ve:
        log.debug("An exception occurred during schema validation!\n" + str(ve))

    return jsonify(resp)


@api.route("/schemas")
def get_schemas():
    """
     @api {get} /schemas Get JSON-schemas.
     @apiVersion 1.0.0
     @apiName GetSchema
     @apiGroup Runner

     @apiPermission api

     @apiSuccess {json} request_schema JSON-Schema, against which all requests to the Runner are validated.
     """
    return jsonify(
        {"request_schema": request_schema, "response_schema": response_schema}
    )

