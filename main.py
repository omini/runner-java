from flask import jsonify, render_template
from openpatch_runner_java import create_app

app = create_app('production')


@app.route('/')
def index():
    """ Generate an index. """

    pages = []
    for rule in app.url_map.iter_rules():
        if 'static' in rule.rule:
            continue
        pages.append(
            [rule.rule, '[' + ', '.join(sorted(list(rule.methods))) + ']'])

    pages.sort()
    pages.insert(0, ["<url>", "<http-methods>"])
    pages = format_table(pages)

    return render_template('index.html', pages=pages)


@app.route('/endpoints')
def get_endpoints():
    return jsonify({"payload": "/api/v1/run/post"})


def format_table(lst, spacing=4):
    """Helper function, to create a nicely formatted table for the index-landing-page."""
    col = []
    col_len = []

    for k in range(0, len(lst[0])):
        col.append([i[k] for i in lst])

    for c in col:
        col_len.append(len(max(c, key=len)) + spacing)

    for l in lst:
        for k in range(0, len(lst[0])):
            while len(l[k]) < col_len[k]:
                l[k] += ' '

    return lst


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, debug=True, port=5001)
