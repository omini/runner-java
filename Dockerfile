FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.6.0

RUN apt-get update && \
    apt-get -y upgrade
RUN apt-get install -y openjdk-11-jdk

COPY "." "/var/www/app"

# Install python dependencies site-wide, since this is an isolated app in a container. We do not make use of a venv.
RUN pip3 install -r /var/www/app/requirements.txt

# We store a copy of the shipped java libraries under /opt to be compliant with FHS
# The 'JavaSDL'-class is expecting them there.
COPY "assets/java-libs" "/opt/java-libs"
