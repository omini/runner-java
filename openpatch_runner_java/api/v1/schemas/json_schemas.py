# This file contains the json-schemas for response and requests handled by commoop-runner-java.
# They can be requested via HTTP-GET at the endpoint '/schemas'.

# Validates all requests, which are sent to commoop-runner-java.
request_schema = {
    "$schema": "http://json-schema.org/draft-04/schema",
    "type": "object",
    "properties": {
        "sources": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "fqcn": "string",
                    "code": "string"
                    },
                "required": ["fqcn", "code"]
                }
            },
        "tests": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "fqcn": "string",
                    "code": "string"
                    },
                "required": ["fqcn", "code"]
                }
            },
    },
    "required": ["sources", "tests"]
}

# Validates all responses, which are sent by commoop-runner-java.
response_schema = {
    "$schema": "http://json-schema.org/draft-04/schema",
    "type": "object",
    "properties": {
        "src_compile": {
            "type": "object",
            "properties": {
                "type": "object",
                "properties": {
                    "call": "string",
                    "exectime": "float",
                    "exitcode": "int",
                    "stdout": "string",
                    "stderr": "string",
                    },
                "required": ["call", "exectime", "exitcode", "stdout", "stderr"]
                }
            },
        "src_run": {
            "type": "object",
            "properties": {
                "type": "object",
                "properties": {
                    "call": "string",
                    "exectime": "float",
                    "exitcode": "int",
                    "stdout": "string",
                    "stderr": "string",
                    },
                "required": ["call", "exectime", "exitcode", "stdout", "stderr"]
                }
            },
        "test_compile": {
            "type": "object",
            "properties": {
                "type": "object",
                "properties": {
                    "call": "string",
                    "exectime": "float",
                    "exitcode": "int",
                    "stdout": "string",
                    "stderr": "string",
                    },
                "required": ["call", "exectime", "exitcode", "stdout", "stderr"]
                }
            },
        "test_run": {
            "type": "object",
            "properties": {
                "type": "object",
                "properties": {
                    "call": "string",
                    "exectime": "float",
                    "exitcode": "int",
                    "stdout": "string",
                    "stderr": "string",
                    },
                "required": ["call", "exectime", "exitcode", "stdout", "stderr"]
                }
            },
        "hmac": {"type": "string", "format": "string"},
        "date": {"type": "string", "format": "date-time"},
    },
    "required": ["src_compile", "src_run", "test_compile", "test_run", "hmac", "date"]
}