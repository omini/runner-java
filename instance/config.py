import os


def get_config():
    # Set the current config:
    return BasicConfig()


class BasicConfig:
    """Parent configuration class."""
    # Disable Flask debugging:
    DEBUG = False

    # Required for AJAX:
    CSRF_ENABLED = True
    CORS_HEADERS = 'Access-Control-Allow-Origin'

    # The HMAC-key is set as an environment variable. It will be passed by commoop-runner on startup. Do not edit.
    OPENPATCH_HMAC_SECRET = os.getenv('OPENPATCH_HMAC_SECRET')

    LOGGER = None

    # Default timeout of all requests, which are sent by "requests".
    # commoop-runner-java does not send any requests at all, yet this variable is necessary for HmacUtils.
    REQUESTS_DEFAULT_TIMEOUT = 20
