import pathlib
import shutil
import time
import unittest

from openpatch_runner_java.api.v1.utils.JavaSDL import JavaSDL
from openpatch_runner_java.api.v1.utils.JavaTools import (JavaToolchainFacade,
                                                          JUnitConverter)
from tests.api_v1_resources import (
    hello_world_src, hello_world_test_fail_src, hello_world_test_ok_src,
    junit_trace_fail, junit_trace_ok1, junit_trace_ok2, junit_trace_ok3)


class TestJavaSDL(unittest.TestCase):
    # Switch me on for debugging output:
    _enablePrintOut = False

    def setUp(self):
        """Creates a sample project, on which further unittests will be executed."""
        self._javasdl = JavaSDL()

        # Create simple classes:
        self._javasdl.write_javafile('HelloWorld', hello_world_src,
                                     JavaSDL.SourceType.SOURCE)
        self._javasdl.write_javafile('HelloWorldTestOk',
                                     hello_world_test_ok_src,
                                     JavaSDL.SourceType.TEST)
        self._javasdl.write_javafile('HelloWorldTestFail',
                                     hello_world_test_fail_src,
                                     JavaSDL.SourceType.TEST)

        # Create single-packaged classes:
        self._javasdl.write_javafile('mypkg1.HelloWorld',
                                     'package mypkg1;\n' + hello_world_src,
                                     JavaSDL.SourceType.SOURCE)
        self._javasdl.write_javafile(
            'mypkg1.HelloWorldTestOk',
            'package mypkg1;\n' + hello_world_test_ok_src,
            JavaSDL.SourceType.TEST)
        self._javasdl.write_javafile(
            'mypkg1.HelloWorldTestFail',
            'package mypkg1\n;' + hello_world_test_fail_src,
            JavaSDL.SourceType.TEST)

        # Create multi-packaged classes:
        self._javasdl.write_javafile(
            'mypkg1.mypkg2.HelloWorld',
            'package mypkg1.mypkg2;\n' + hello_world_src,
            JavaSDL.SourceType.SOURCE)
        self._javasdl.write_javafile(
            'mypkg1.mypkg2.HelloWorldTestOk',
            'package mypkg1.mypkg2;\n' + hello_world_test_ok_src,
            JavaSDL.SourceType.TEST)
        self._javasdl.write_javafile(
            'mypkg1.mypkg2.HelloWorldTestFail',
            'package mypkg1.mypkg2;\n' + hello_world_test_fail_src,
            JavaSDL.SourceType.TEST)

        if self._enablePrintOut:
            print("Base dir is: " + self._javasdl.get_base_dir())
        # Sleep a little bit since we use system time to create base directories
        time.sleep(0.001)

    def test_basepath_dir_creation(self):
        """Tests the creation of the base path, i.e. the session folder."""
        path_dir = pathlib.Path(self._javasdl.get_base_dir())
        self.assertEqual(path_dir.is_dir(), True)

    def test_no_package_src_class_dir_creation(self):
        """Test the creation of a javasdl-directory in the default package."""
        path_dir = pathlib.Path(self._javasdl.get_src_dir())
        self.assertEqual(path_dir.is_dir(), True)

    def test_no_package_src_class_file_creation(self):
        """Tests the creation of a source file within the java standard directory layout within the default package."""
        path_file = pathlib.Path(self._javasdl.get_src_dir() +
                                 'HelloWorld.java')
        self.assertEqual(path_file.is_file(), True)

    def test_no_package_test_dir_creation(self):
        """Tests the creation of a test within the java standard directory layout."""
        path_dir = pathlib.Path(self._javasdl.get_test_dir())
        self.assertEqual(path_dir.is_dir(), True)

    def test_no_package_test_class_file_creation(self):
        """Tests the creation of some class source files within the java sdl."""
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'HelloWorldTestOk.java')
        self.assertEqual(path_file.is_file(), True)
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'HelloWorldTestFail.java')
        self.assertEqual(path_file.is_file(), True)

    def test_single_package_src_class_dir_creation(self):
        """Tests the creation of a package-directory."""
        path_dir = pathlib.Path(self._javasdl.get_src_dir() + 'mypkg1/')
        self.assertEqual(path_dir.is_dir(), True)

    def test_single_package_src_class_file_creation(self):
        """Tests the creation of a javasdl-sourcefile within a single package."""
        path_file = pathlib.Path(self._javasdl.get_src_dir() +
                                 'HelloWorld.java')
        self.assertEqual(path_file.is_file(), True)

    def test_single_package_test_dir_creation(self):
        """Tests the creation of a javasdl-test-directory within a single package."""
        path_dir = pathlib.Path(self._javasdl.get_test_dir() + 'mypkg1/')
        self.assertEqual(path_dir.is_dir(), True)

    def test_single_package_test_class_file_creation(self):
        """Tests the creation of a javasdl-testfile within a single package."""
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'mypkg1/HelloWorldTestOk.java')
        self.assertEqual(path_file.is_file(), True)
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'mypkg1/HelloWorldTestFail.java')
        self.assertEqual(path_file.is_file(), True)

    def test_multi_package_src_dir_creation(self):
        """Tests the creation of a javasdl-source-directory within multiple packages."""
        path_dir = pathlib.Path(self._javasdl.get_src_dir() + 'mypkg1/mypkg2/')
        self.assertEqual(path_dir.is_dir(), True)

    def test_multi_package_src_class_file_creation(self):
        """Tests the creation of a javasdl-source-file within multiple packages."""
        path_file = pathlib.Path(self._javasdl.get_src_dir() +
                                 'mypkg1/mypkg2/HelloWorld.java')
        self.assertEqual(path_file.is_file(), True)

    def test_multi_package_test_dir_creation(self):
        """Tests the creation of javasdl-source-directory within multiple packages."""
        path_dir = pathlib.Path(self._javasdl.get_test_dir() +
                                'mypkg1/mypkg2/')
        self.assertEqual(path_dir.is_dir(), True)

    def test_multi_package_test_class_file_creation(self):
        """Tests the creation of javasdl-test-files within multiple packages."""
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'mypkg1/mypkg2/HelloWorldTestOk.java')
        self.assertEqual(path_file.is_file(), True)
        path_file = pathlib.Path(self._javasdl.get_test_dir() +
                                 'mypkg1/mypkg2/HelloWorldTestFail.java')
        self.assertEqual(path_file.is_file(), True)

    def test_get_classpath(self):
        """Tests, whether the global-jars are in classpath."""
        base_dir = self._javasdl.get_base_dir()
        cp = self._javasdl.get_classpath()
        self.assertIn('mypkg1/mypkg2', cp)
        self.assertIn(base_dir, cp)
        self.assertIn(
            'hamcrest-core-1.3.jar',
            cp,
            msg='Unittest failed. Have you updated hamcrest-core?')
        self.assertIn(
            'junit-4.12.jar',
            cp,
            msg='Unittest failed. Have you updated junit?')

    def test_fqcn_to_path(self):
        """Tests the conversion of fqcn to path."""
        self.assertEqual(self._javasdl.fqcn_to_path("HelloWorld"), "")
        self.assertEqual(
            self._javasdl.fqcn_to_path("pkg1.HelloWorld"), "pkg1/")
        self.assertEqual(
            self._javasdl.fqcn_to_path("pkg1.pkg2.HelloWorld"), "pkg1/pkg2/")

    def test_get_java_filename(self):
        """Tests the conversion of fqcn to filenames."""
        self.assertEqual(
            self._javasdl.get_java_filename("HelloWorld"), "HelloWorld.java")
        self.assertEqual(
            self._javasdl.get_java_filename("pkg1.HelloWorld"),
            "HelloWorld.java")
        self.assertEqual(
            self._javasdl.get_java_filename("pkg1.pkg2.HelloWorld"),
            "HelloWorld.java")

    def get_jsdl(self):
        return self._javasdl

    def tearDown(self):
        if self._enablePrintOut:
            print("Removing base dir: " + self._javasdl.get_base_dir())
        shutil.rmtree(self._javasdl.get_base_dir())


class TestJavaTools(unittest.TestCase):
    def setUp(self):
        self._testjavasdl = TestJavaSDL()
        self._testjavasdl.setUp()
        self._jsdl = self._testjavasdl.get_jsdl()
        self._jtf = JavaToolchainFacade()

    def test_no_package_compile_run(self):
        """Tests, whether a a file is correctly compiled and executed in the default package."""
        # Check whether javac exits with returncode == 0
        ret = self._jtf.compile_javac(self._jsdl, "HelloWorld",
                                      JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl, "HelloWorldTestOk",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl, "HelloWorldTestFail",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])

        # Run files:
        ret = self._jtf.run(self._jsdl, "HelloWorld",
                            JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "HelloWorldTestOk",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "HelloWorldTestFail",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 1, msg=ret['stdout'])

    def test_single_package_compile_run(self):
        """Tests, whether a a file is correctly compiled and executed in a single package."""
        ret = self._jtf.compile_javac(self._jsdl, "mypkg1.HelloWorld",
                                      JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl, "mypkg1.HelloWorldTestOk",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl, "mypkg1.HelloWorldTestFail",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])

        ret = self._jtf.run(self._jsdl, "mypkg1.HelloWorld",
                            JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "mypkg1.HelloWorldTestOk",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "mypkg1.HelloWorldTestFail",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 1, msg=ret['stdout'])

    def test_multi_package_compile_run(self):
        """Tests, whether a a file is correctly compiled and executed in multiple packages."""
        ret = self._jtf.compile_javac(self._jsdl, "mypkg1.mypkg2.HelloWorld",
                                      JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl,
                                      "mypkg1.mypkg2.HelloWorldTestOk",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.compile_javac(self._jsdl,
                                      "mypkg1.mypkg2.HelloWorldTestFail",
                                      JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])

        ret = self._jtf.run(self._jsdl, "mypkg1.mypkg2.HelloWorld",
                            JavaSDL.SourceType.SOURCE)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "mypkg1.mypkg2.HelloWorldTestOk",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 0, msg=ret['stdout'])
        ret = self._jtf.run(self._jsdl, "mypkg1.mypkg2.HelloWorldTestFail",
                            JavaSDL.SourceType.TEST)
        self.assertEqual(ret['exitcode'], 1, msg=ret['stdout'])

    def tearDown(self):
        self._testjavasdl.tearDown()


class TestJUnitConverter(unittest.TestCase):
    def setUp(self):
        self._tjc = JUnitConverter()

    def test_ok_trace(self):
        """Tests, whether all RegExes are correctly evaluated, when no error occured."""
        a = self._tjc.convert_junit_output(junit_trace_ok1)
        b = self._tjc.convert_junit_output(junit_trace_ok2)
        c = self._tjc.convert_junit_output(junit_trace_ok3)

        dict_a = {
            'total_tests_failed': 0,
            'values': [],
            'is_correct': True,
            'total_tests_run': 1
        }
        dict_b = {
            'total_tests_failed': 0,
            'values': [],
            'is_correct': True,
            'total_tests_run': 42
        }
        dict_c = {
            'total_tests_failed': 0,
            'values': [],
            'is_correct': True,
            'total_tests_run': 9999
        }

        self.assertEqual(a, dict_a)
        self.assertEqual(b, dict_b)
        self.assertEqual(c, dict_c)

    def test_fail_trace(self):
        """Tests, whether all RegExes are correctly evaluated, when some/only error(s) occured."""
        a = self._tjc.convert_junit_output(junit_trace_fail)

        dict_a = {
            'total_tests_failed':
            3,
            'is_correct':
            False,
            'total_tests_run':
            4,
            'values': [
                {
                    'message':
                    'This is the result message for my first failing test.',
                    'expected_value':
                    'Hello[Universe]',
                    'actual_value':
                    'Hello[World]',
                    'number':
                    1
                },
                {
                    'message':
                    'This is the result message for my second failing test.',
                    'expected_value':
                    'Hello[Cosmos]',
                    'actual_value':
                    'Hello[World]',
                    'number':
                    2
                },
                {
                    'message': '',  # The third test has no message.
                    'expected_value': 'Hello[Cosmos]',
                    'actual_value': 'Hello[World]',
                    'number': 3
                }
            ]
        }
        self.assertEqual(a, dict_a)


if __name__ == '__main__':
    unittest.main()
