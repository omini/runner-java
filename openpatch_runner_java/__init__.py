# Local imports:
from flask import Flask
from instance.config import get_config
from openpatch_runner_java.api.v1 import api as api_v1


def create_app(config_name):
    """Creates an app and registers the /api/v1/-blueprint."""
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(get_config())

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/api/v1")

    return app
